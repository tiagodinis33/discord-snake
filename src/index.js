const discordjs = require("discord.js");
const snakeGame = require("./game/snakeGame")
const client = new discordjs.Client();

const commands = {
    w: async (game) => {
        await sendMessageIfGameOver(game,game.moveY(-1))
        game.msg.edit(game.getGameState());
    },
    s: async (game) => {
        await sendMessageIfGameOver(game,game.moveY(1))
        game.msg.edit(game.getGameState());
    },
    
    a: async (game) => {
        await sendMessageIfGameOver(game,game.moveX(-1))
        game.msg.edit(game.getGameState());
    },
    d: async (game) => {
        await sendMessageIfGameOver(game,game.moveX(1))
        game.msg.edit(game.getGameState());
    },
    
}
client.on("ready", () => {
    console.log("Ready!");
});

client.on("messageReactionAdd", async (e, user)=>{
    if(!e.me && games[user.id] && e.message.equals(games[user.id].msg)){
        const game = games[user.id];
        const control = commands[(await (e.users.remove(user.id))).emoji.name];
        if(control){
            control(game);
        }
    }
})
const games = {

}
async function sendMessageIfGameOver(game ,losed){
    if(losed){
        await game.msg.channel.send(new discordjs.MessageEmbed().setTitle("GAME OVER!").setColor(0xff0000))
        games[game.player.id] = undefined;
    }
}
commands["🔼"] = commands.w;
commands["🔽"] = commands.s;
commands["◀️"] = commands.a;
commands["▶️"] = commands.d;
const dc = {
    "->start": async (msg) => {
        let game = new snakeGame.Game();
        
        game.msg = await msg.channel.send(game.getGameState());
        game.msg.react("🔼");
        game.msg.react("🔽");
        game.msg.react("◀️");
        game.msg.react("▶️");
        game.player = msg.author;
        games[msg.author.id] = game;

    },
    "->stop": async (msg) => {
        games[msg.author.id] = undefined;
        msg.channel.send(new discordjs.MessageEmbed().setTitle("Você parou o jogo!").setColor(0xff0000));
    }
}
client.on("message", async msg => {
    if(commands[msg.content] && games[msg.author.id]){
        const control = commands[msg.content];
        if(control){
            await control(games[msg.author.id]);
            await msg.delete();
        }
        return;
    } 
    const command = dc[msg.content.split(" ")[0]];
    if(command){
        await command(msg);
    }
        
})
client.login(process.env.TOKEN);
