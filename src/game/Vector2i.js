class Vector2i {
    x = 0;
    y = 0;
    Vector2i() { }
    equals(other) {
        return (other.x === this.x && other.y === this.y);
    }
    static getRandom(maxX, maxY) {
        return {
            x: Math.floor(Math.random() * maxX), y: Math.floor(Math.random() * maxY),
            equals(other) {
                return (other.x === this.x && other.y === this.y);
            }
        };
    }
}
module.exports = Vector2i;