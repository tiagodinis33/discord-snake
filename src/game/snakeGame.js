const Vector2i = require("./Vector2i");
class Theme {
    backg = "🟫";
    snake = "🟠";
    snakeTail = "🟡";
    apple = "🍎";
}

const width = 12;
const height = 8;
class Game {
    lastPositions = [];
    position = Vector2i.getRandom(width, height);
    theme = new Theme();
    apple = Vector2i.getRandom(width, height);
    tail = 1;
    getGameState() {
        var str = [];
        for (var y = 0; y < height; y++) {

            for (var x = 0; x < width; x++) {
                str.push(this.getTileEmoji({
                    x,
                    y,
                    equals(other) {
                        return (other.x === this.x && other.y === this.y);
                    }
                }
                ));
            }
            str.push("\n");
        }
        return str.join("");
    }
    increaseTail(amount) {
        this.tail++;
    }
    getTileEmoji(pos) {
        for (let i = 0; i < this.lastPositions.length; i++) {
            if (pos.equals(this.lastPositions[i])) {
                return this.theme.snakeTail;
            }

        }
        if (pos.equals(this.position)) {
            return this.theme.snake;
        }
        if (pos.equals(this.apple)) {
            return this.theme.apple;
        }
        return this.theme.backg;
    }
    checkIfColidedWithTail(pos){
        for (let i = 0; i < this.lastPositions.length; i++) {
            if (pos.equals(this.lastPositions[i])) {
                return true;
            }

        }
    }
    moveY(sign) {
        if (this.lastPositions[0] && this.lastPositions[0].y == this.position.y + Math.sign(sign)) return false;
        this.updateLastPositions();
        if(this.checkIfColidedWithTail({
            x: this.position.x,
            y: this.position.y+Math.sign(sign),
            equals: this.position.equals
        })) return true;
        this.position = {
            x: this.position.x,
            y: this.position.y+Math.sign(sign),
            equals: this.position.equals
        };
        this.checkCollidedWithFruit();
        return this.checkCollidedWithWall();
    }
    checkCollidedWithFruit(){
        if(this.position.equals(this.apple)){
            this.apple = Vector2i.getRandom(width, height);
            this.tail++;
        }
    }
    checkCollidedWithWall(){
        return this.position.x >= width || this.position.y >= height || this.position.x < 0 || this.position.y < 0;
    }
    moveX(sign) {
        if (this.lastPositions[0] && this.lastPositions[0].x ==( this.position.x + Math.sign(sign))) return false;
        this.updateLastPositions();
        if(this.checkIfColidedWithTail({
            x: this.position.x+Math.sign(sign),
            y: this.position.y,
            equals: this.position.equals
        })) return true;
        this.position = {
            x: this.position.x+Math.sign(sign),
            y: this.position.y,
            equals: this.position.equals
        };
        this.checkCollidedWithFruit();
        return this.checkCollidedWithWall();
    }

    updateLastPositions() {
        if (this.tail == 0)
            this.lastPositions.pop();
        else {
            this.tail--;
        }
        this.lastPositions.unshift(this.position);

    }
}
module.exports = {
    Game,
    Theme
};